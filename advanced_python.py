import Bio
import sys

from Bio import SwissProt
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO
from Bio.Align.Applications import ClustalwCommandline
from Bio import Phylo

userInterPro = str(sys.argv[1])
userOrganism = str(sys.argv[2])
  
outputdirectory = "/home/manager/element_tree/arctic_output/"
allSeqs = []
interpro_id = ["IPR016174"]
organisms_name = ["orcinus orca (killer whale).","dicrostonyx torquatus (arctic lemming).","spheniscus magellanicus (magellanic penguin).","phoenicopterus ruber (carribean flamingo)."]


for record in SwissProt.parse(open("/home/manager/uniprot_sprot.dat")):
       add = False
       for i in range(len(record.cross_references)):
              if record.cross_references[i][1] == userInterPro  and record.organism.lower() == userOrganism.lower():
                     add = True  
              if record.cross_references[i][1] in interpro_id and record.organism.lower() in organisms_name:
                     add = True               
       if add:
              rec = SeqRecord(Seq(record.sequence, record.gene_name), id = record.entry_name, description=record.description )                  
              allSeqs.append(rec) 
              print (rec)
              print (record.organism)
              
SeqIO.write(allSeqs, outputdirectory+"allSeq.faa", "fasta")

cline = ClustalwCommandline("clustalw", infile="/home/manager/element_tree/arctic_output/allSeq.faa", outfile = "/home/manager/element_tree/arctic_output/clustal.txt")
cline()

tree = Phylo.read(outputdirectory+"allSeq.dnd", "newick")
Phylo.draw_ascii(tree)
